package Bank_1;

public class BankAccount {
	private String name;
	private double balance;
	
	public BankAccount(String name) {
		this.name = name;
		this.balance = 0;
	}
	
	public String getName() {
		return this.name;
	}
	
	public double getBalance() {
		return this.balance;
	}
	
	public void deposite(double amount) {
		this.balance += amount;
	}
	
	public void withdraw(double amount) {
		this.balance -= amount;
	}
	
	public void transfer(BankAccount b1,double amount) {
		withdraw(amount);
		b1.deposite(amount);
	}

}

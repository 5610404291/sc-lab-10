package GUIRadio;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.*;

public class RadioRGB extends JFrame {
	private JFrame frame;
	private JRadioButton r,g,b;
	private JPanel r1,b1,g1;
	private JPanel main,button,cardPanel;
	private CardLayout layoutmain;
	private ButtonGroup group;
	
	public RadioRGB() {
		createframe();
	}
	
	public void createframe() {
		frame = new JFrame();
		frame.setSize(1000, 700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		r = new JRadioButton("R");
		g = new JRadioButton("G");
		b = new JRadioButton("B");
		group = new ButtonGroup();
		group.add(r);
		group.add(g);
		group.add(b);
		main = new JPanel();
		main.setLayout(new BorderLayout());
		button = new JPanel();
		GridLayout layoutChooseCustomer = new GridLayout();
		layoutChooseCustomer.setColumns(3);
		button.setLayout(layoutChooseCustomer);
		button.add(r);
		button.add(g);
		button.add(b);
		r.setSelected(true);
		g.setSelected(false);
		b.setSelected(false);
		
		
		main.add(button, BorderLayout.SOUTH);
		
		layoutmain = new CardLayout();
		cardPanel = new JPanel();
		cardPanel.setLayout(layoutmain);
		
		r1 = new JPanel();
		r1.setBackground(new Color(255, 0, 0));
		g1 = new JPanel();
		g1.setBackground(new Color(0, 255, 0));
		b1 = new JPanel();
		b1.setBackground(new Color(0, 0, 255));
		
		cardPanel.add(r1,"R");
		cardPanel.add(g1,"G");
		cardPanel.add(b1,"B");
		
		main.add(cardPanel,BorderLayout.CENTER);
		frame.getContentPane().add(main);
		
		
	}
	
	public void setListener(ActionListener addlistlistener) {
		r.addActionListener(addlistlistener);
		g.addActionListener(addlistlistener);
		b.addActionListener(addlistlistener);
		
	}
	
	public CardLayout getLayout() {
		return layoutmain;
	}
	public JPanel getCardpanel() {
		return cardPanel;
	}

}

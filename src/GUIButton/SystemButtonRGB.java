package GUIButton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SystemButtonRGB {
	buttonRGB frame1;
	
	public SystemButtonRGB() {
		frame1 = new buttonRGB();
		frame1.setListener(new addlistListener());
		
	}
	
	class addlistListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent ae) {
			// TODO Auto-generated method stub
			switch(ae.getActionCommand()) {
			case  "R" :
				frame1.getLayout().show(frame1.getCardpanel(), "R");
				break;
			case "G" :
				frame1.getLayout().show(frame1.getCardpanel(), "G");
				break;
			case "B" :
				frame1.getLayout().show(frame1.getCardpanel(), "B");
				break;
			}
		}
	}

}

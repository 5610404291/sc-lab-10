package GUICheckbox;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;

public class SystemCheckBoxRGB {
	CheckboxRBG frame;
	
	public SystemCheckBoxRGB() {
		frame = new CheckboxRBG();
		frame.setListener(new CheckBoxActionListener());
	}
	class CheckBoxActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent ae) {
			// TODO Auto-generated method stub
	        if (frame.getR().isSelected() && !frame.getG().isSelected() && !frame.getB().isSelected()) {
	        	frame.getLayout().show(frame.getCardpanel(), "R");
	        } 
	        else if (!frame.getR().isSelected() && frame.getG().isSelected() && !frame.getB().isSelected()) {
	        	frame.getLayout().show(frame.getCardpanel(), "G");
	        } 
	        else if (!frame.getR().isSelected() && !frame.getG().isSelected() && frame.getB().isSelected()) {
	        	frame.getLayout().show(frame.getCardpanel(), "B");
	        }
	        else if (!frame.getR().isSelected() && !frame.getG().isSelected() && !frame.getB().isSelected()) {
	        	frame.getLayout().show(frame.getCardpanel(), "Default");
	        }
	        else if (frame.getR().isSelected() && frame.getG().isSelected() && !frame.getB().isSelected()) {
	        	frame.getLayout().show(frame.getCardpanel(), "RG");
	        }
	        else if (frame.getR().isSelected() && !frame.getG().isSelected() && frame.getB().isSelected()) {
	        	frame.getLayout().show(frame.getCardpanel(), "RB");
	        }
	        else if (!frame.getR().isSelected() && frame.getG().isSelected() && frame.getB().isSelected()) {
	        	frame.getLayout().show(frame.getCardpanel(), "GB");
	        }
	        else if (frame.getR().isSelected() && frame.getG().isSelected() && frame.getB().isSelected()) {
	        	frame.getLayout().show(frame.getCardpanel(), "RGB");
	        }
		}
	}

}

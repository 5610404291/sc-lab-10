package GUICombobox;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SystemComboRGB {
	ComboBoxRGB frame;
	
	public SystemComboRGB() {
		frame = new ComboBoxRGB();
		frame.setListener(new ComboBoxActionListener());
	}
	class ComboBoxActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent ae) {
			// TODO Auto-generated method stub
			String s = (String) frame.getBox().getSelectedItem();
			switch(s) {
			case  "RED" :
				frame.getLayout().show(frame.getCardpanel(), "R");
				break;
			case "GREEN" :
				frame.getLayout().show(frame.getCardpanel(), "G");
				break;
			case "BLUE" :
				frame.getLayout().show(frame.getCardpanel(), "B");
				break;
				
			}
		}
	}

}
